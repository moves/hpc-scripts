#!/usr/bin/env zsh

### Project name. Required to actually use *our* cluster
#SBATCH -A moves

### Job name
#SBATCH --job-name=benchpy

### Array job
#SBATCH --array=0-9

### Output file name. Zero-pad task id with 4
#SBATCH -o /home/%u/output/%x.%A_%4a.out

### Error file name. Zero-pad task id with 4. If omitted, stderr will also be written in the output file
# // #SBATCH -e /home/%u/output/JOB.%A_%4a.err

### Timelimit in hours:minutes:seconds
#SBATCH -t 01:20:00

### Number of available threads
#SBATCH --cpus-per-task=8

### Memory per thread in Megabytes. Total amount of available memory is --cpus-per-task * --mem-per-cpu
#SBATCH --mem-per-cpu=512M

### Receive all kinds of email notifications
# // #SBATCH --mail-type=ALL
# // #SBATCH --mail-user=your.name@cs.rwth-aachen.de

### Load the module system configuration, necessary to load dependent libraries
source $HOME/hpc-scripts/prepareEnvironmentGcc.sh 

### Change Directory to your working directory (binaries, etc)
cd $HOME/hpc-scripts/examples/

start=`date`
echo "Starting task $SLURM_ARRAY_TASK_ID at $start"
python call_python_script.py $SLURM_ARRAY_TASK_ID ;rc=$?
end=`date`
echo "Finished task $SLURM_ARRAY_TASK_ID with exit code $rc at $end"

