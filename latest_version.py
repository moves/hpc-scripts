#! env python3

import os
import re
import sys

version_re = re.compile("[0-9._-]+")
def is_version(s):
    return version_re.match(s)

def get_latest_version(module):
    BASEDIR = "/usr/local_rwth/modules/modulefiles/linux/x86-64/%s" % module
    dirs = sorted(filter(lambda a: is_version(a) and not a[0] == '.', os.listdir(BASEDIR)))
    return dirs[-1]

tasks = {
    "boost": lambda : get_latest_version("LIBRARIES/boost"),
    "clang": lambda : get_latest_version("DEVELOP/clang"),
    "cmake": lambda : get_latest_version("DEVELOP/cmake"),
    "gcc": lambda : get_latest_version("DEVELOP/gcc"),
}

print(tasks[sys.argv[1]]())
