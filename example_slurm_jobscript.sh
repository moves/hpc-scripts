#!/usr/bin/env zsh

### Job name
#SBATCH --job-name=benchmax

### Output file name
#SBATCH -o /work/gk809425/CAD_IFH_PH_PP_19-06-24_15:57/JOB.%A_%a.out

### Error file
#SBATCH -e /work/gk809425/CAD_IFH_PH_PP_19-06-24_15:57/JOB.%A_%a.err

### Timelimit in hours:minutes:seconds
#SBATCH -t 00:25:00

### Memory per CPU
#SBATCH --mem-per-cpu=5120M

### Load the module system configuration, necessary to load dependent libraries
source ~/load_environment

### Change Directory to your working directory (binaries, etc)
cd /work/gk809425/CAD_IFH_PH_PP_19-06-24_15:57

### For Array Job striping - the slicesize is the number of jobs executed in a single slice (running process) in serial
min=$SLURM_ARRAY_TASK_MIN
max=$SLURM_ARRAY_TASK_MAX
cur=$SLURM_ARRAY_TASK_ID
slicesize=6
start=$(( (cur - 1) * slicesize + 1 ))
end=$(( start + slicesize - 1 ))
for i in `seq ${start} ${end}`; do
        cmd=$(time sed -n "${i}p" < /work/gk809425/CAD_IFH_PH_PP_19-06-24_15:57/jobs-1561695618-1.jobs)
        echo "Executing $cmd"
        echo "# START ${i} #"
        echo "# START ${i} #" >&2
        cat ${cmd} > /dev/null
        start=`date +"%s%3N"`
        ulimit -c 0 && ulimit -S -v 4194304 && ulimit -S -t 123 && eval time $cmd ; rc=$?
        end=`date +"%s%3N"`
        echo "# END ${i} #"
        echo "# END ${i} #" 1>&2
        echo "time: $(( end - start ))"
        echo "exitcode: $rc"
        echo "# END DATA ${i} #"
done

### Ulimit:
###  -c 0: No core dumps (important!)
###  -v N: Limit to N kilobytes of memory
###  -t N: Limit of CPU time for this subprocess