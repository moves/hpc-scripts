#!/bin/bash

module add DEVELOP
module add LIBRARIES
module load MATH

if [[ -z "$BASH_SOURCE" ]]; then
	script_dir=$0
else
	script_dir=${BASH_SOURCE[0]}
fi
echo "Script dir: $script_dir"

root_folder=$(dirname "$(readlink -f $script_dir)")

boostCurrentVersion=$(php $root_folder/moduleTools/getMostRecentVersion.php boost)
clangCurrentVersion=$(php $root_folder/moduleTools/getMostRecentVersion.php clang)
cmakeCurrentVersion=$(php $root_folder/moduleTools/getMostRecentVersion.php cmake)
gccCurrentVersion=$(php $root_folder/moduleTools/getMostRecentVersion.php gcc)

echo "Using Boost version $boostCurrentVersion"
echo "Using CMake version $cmakeCurrentVersion"
echo "Using Clang version $clangCurrentVersion"

# This is the Clang version
module switch intel clang/$clangCurrentVersion

module add DEVELOP
module add LIBRARIES
module load MATH

module load cmake/$cmakeCurrentVersion
module load LIBRARIES boost/$boostCurrentVersion
module load MATH gurobi
module load inteltbb
module load intelmkl

