<?php

$output = '';
if (count($argv) >= 2) {
	//echo "Error: Missing argument!\nRequired argument: filename, containing the output of cpuinfo.\n";
	//exit;
	$output = file($argv[1]);
} else {
	$lines = array();
	$returnVal = -1;
	exec('cat /proc/cpuinfo', $lines, $returnVal);
	if ($returnVal != 0) {
		echo "Error: Could not get CPUinfo!\n";
		exit;
	}
	//echo "ReturnVal = ".$returnVal.".\n";
	$output = $lines;
}

//$file = file($argv[1]);
//var_dump($file);

// fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc 
// arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx smx est tm2 ssse3 
// sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm invpcid_single kaiser 
// tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid xsaveopt dtherm ida arat pln pts

// fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc 
// arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf pni dtes64 monitor ds_cpl vmx est tm2 ssse3 cx16 xtpr pdcm dca sse4_1 sse4_2 
// x2apic popcnt lahf_lm epb tpr_shadow vnmi flexpriority ept vpid dtherm ida


$cpus = array(
	'core2' => array('mmx', 'sse', 'sse2', 'ssse3'),
	'nehalem' => array('mmx', 'sse', 'sse2', 'ssse3', 'sse4_1', 'sse4_2', 'popcnt'),
	'westmere' => array('mmx', 'sse', 'sse2', 'ssse3', 'sse4_1', 'sse4_2', 'popcnt', 'aes', 'pclmulqdq'),
	'sandybridge' => array('mmx', 'sse', 'sse2', 'ssse3', 'sse4_1', 'sse4_2', 'popcnt', 'aes', 'pclmulqdq', 'avx'),
	'ivybridge' => array('mmx', 'sse', 'sse2', 'ssse3', 'sse4_1', 'sse4_2', 'popcnt', 'aes', 'pclmulqdq', 'avx', 'fsgsbase', 'rdrand', 'f16c'),
	'haswell' => array('mmx', 'sse', 'sse2', 'ssse3', 'sse4_1', 'sse4_2', 'popcnt', 'aes', 'pclmulqdq', 'avx', 'fsgsbase', 'rdrand', 'f16c', 'movbe', 'avx2', 'fma', 'bmi1', 'bmi2'),
	'broadwell' => array('mmx', 'sse', 'sse2', 'ssse3', 'sse4_1', 'sse4_2', 'popcnt', 'aes', 'pclmulqdq', 'avx', 'fsgsbase', 'rdrand', 'f16c', 'movbe', 'avx2', 'fma', 'bmi1', 'bmi2', 'rdseed', 'adcx', 'prefetchw'),
	'skylake' => array('mmx', 'sse', 'sse2', 'ssse3', 'sse4_1', 'sse4_2', 'popcnt', 'aes', 'pclmulqdq', 'avx', 'fsgsbase', 'rdrand', 'f16c', 'movbe', 'avx2', 'fma', 'bmi1', 'bmi2', 'rdseed', 'adcx', 'prefetchw', 'clflushopt', 'xsavec', 'xsaves'),	
);

$lastArch = '';
foreach ($output as $line) {
	if (strpos($line, 'flags') === 0) {
		$parts = explode(':', $line);
		$flags = explode(' ', $parts[1]);
		//echo "Found ".count($flags)." flags.\n";
		
		foreach ($cpus as $name => $features) {
			$canBe = true;
			foreach ($features as $feature) {
				if (array_search($feature, $flags) === FALSE) {
					$canBe = false;
				}
			}
			if ($canBe) {
				$lastArch = $name;
				//echo "Could be CPU arch \"".$name."\"...\n";
			}
		}
		
		break;
	}
}

//echo "Done.\n";
echo $lastArch;
