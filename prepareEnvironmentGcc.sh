#!/bin/bash

module add DEVELOP
module add LIBRARIES
module load MATH

if [[ -z "$BASH_SOURCE" ]]; then
	script_dir=$0
else
	script_dir=${BASH_SOURCE[0]}
fi
echo "Script dir: $script_dir"

root_folder=$(dirname "$(readlink -f $script_dir)")

boostCurrentVersion=$(php $root_folder/moduleTools/getMostRecentVersion.php boost)
clangCurrentVersion=$(php $root_folder/moduleTools/getMostRecentVersion.php clang)
cmakeCurrentVersion=$(php $root_folder/moduleTools/getMostRecentVersion.php cmake)
gccCurrentVersion=$(php $root_folder/moduleTools/getMostRecentVersion.php gcc)
gurobiCurrentVersion=$(php $root_folder/moduleTools/getMostRecentVersion.php gurobi)

echo "Using Boost version $boostCurrentVersion"
echo "Using CMake version $cmakeCurrentVersion"
echo "Using GCC version $gccCurrentVersion"
echo "Using Gurobi version $gurobiCurrentVersion"

# This is the GCC version
module switch intel gcc/$gccCurrentVersion

module add DEVELOP
module add LIBRARIES
module load MATH

module load cmake/$cmakeCurrentVersion
module load LIBRARIES boost/$boostCurrentVersion
module load MATH gurobi/$gurobiCurrentVersion
module load inteltbb
module load intelmkl

