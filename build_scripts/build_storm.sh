#!/bin/bash

# Stop on first error. This is considered bad practice but it makes life easier.
set -e

# Print usage
if [[ "$#" < 1 || "$#" > 3 ]]; then
        echo "# Script for building Storm and its dependencies (Z3, Carl) on the cluster."
        echo "#"
	echo "# Usage: ./build_storm.sh DIRECTORY [GIT_USER_NAME] [GIT_BRANCH]"
	echo "# - DIRECTORY      specifies the folder in which all tools are built."
	echo "# - GIT_USER_NAME  is optional. If present, the internal SCM Git with the given username is used."
        echo "#                  Otherwise, the public Github repo is used."
	echo "# - GIT_BRANCH     is optional. If present, the given branch is used instead of the default master branch."
        echo "#                  Note that GIT_USER_NAME has to be given in this instance as the internal SCM Git is used."    
	exit 1
fi

# Variables
DIR=$(readlink -f $1)
USER=$2
BRANCH=${3:-master}
CORES=40
SCRIPT_DIR=$(dirname "$(readlink -f "$0")")

# Output given configuration
echo "# Building Storm and its dependencies in directory '$DIR'."
if [ -z "$USER" ]; then
	echo "# Using public GitHub repos."
else
	echo "# Using SCM Git repos with user '$USER'."
fi
echo "# Using Storm branch '$BRANCH'."

# Create DIR
mkdir -p $DIR


# Prepare cluster environment (gcc, boost, etc.)
echo "# Preparing evironment."
source $SCRIPT_DIR/../prepareEnvironmentGcc.sh


# IntelTBB
# Get correct library paths for Intel TBB
TBB_PREFIX=$(tr ':' '\n' <<< "$LD_LIBRARY_PATH" | grep $TBBROOT | tr '\n' ';')
TBB_PREFIX="$TBB_PREFIX$TBBROOT" # we need no ; separator here because the tr command already added a trailing semicolon
echo "# TBB Prefix: $TBB_PREFIX"

# Cloning all required repos
# We only need to ask for the passwords in the beginning and the remaining build process then works without interaction

# Cloning Carl
echo "# Cloning Carl."
cd $DIR
if [ ! -d carl ]; then
	# Use correct git repo
	if [ -z "$USER" ]; then
		git clone https://github.com/smtrat/carl.git -b master14
	else
		git clone https://$USER@srv-i2.informatik.rwth-aachen.de/scm/git/carl.git -b master14
	fi
fi


# Cloning Storm
echo "# Cloning Storm."
cd $DIR
if [ ! -d storm ]; then
	# Use correct git repo
	if [ -z "$USER" ]; then
		git clone https://github.com/moves-rwth/storm.git -b $BRANCH
	else
		git clone https://$USER@srv-i2.informatik.rwth-aachen.de/scm/git/storm.git -b $BRANCH
	fi
fi


# Cloning and building Z3
echo "# Cloning Z3."
cd $DIR
if [ ! -d z3_src ]; then
	git clone https://github.com/Z3Prover/z3.git z3_src
	cd z3_src
	# Get latest tag
	latestTag=$(git tag -l | sort -Vf | tail -1) # Sorted alphabetically and ignore case
	echo "# Using Z3 tag $latestTag"
	git checkout $latestTag
fi
echo "# Building Z3."
cd $DIR
if [ ! -d z3 ]; then
	cd z3_src
	python scripts/mk_make.py --prefix=$DIR/z3
	cd build
	make -j$CORES
	make install -j$CORES
fi
echo "# Building Z3 finished."


# Carl
echo "# Building Carl."
cd $DIR/carl
mkdir -p build
cd build
echo "$BOOST_ROOT"
if [ ! -f CMakeCache.txt ]; then
	# Carl CMake configure command
	cmake .. -DCMAKE_BUILD_TYPE=RELEASE \
		-DUSE_CLN_NUMBERS=ON -DUSE_GINAC=ON \
		-DBOOST_ROOT="$BOOST_ROOT" \
		-DTHREAD_SAFE=ON
fi
make lib_carl -j$CORES
echo "# Building Carl finished."


# Storm
echo "# Building Storm."
cd $DIR/storm
mkdir -p build
cd build
if [ ! -f CMakeCache.txt ]; then
	# Storm CMake configure command
	cmake .. -DCMAKE_BUILD_TYPE=Release -DSTORM_DEVELOPER=OFF -DSTORM_LOG_DISABLE_DEBUG=ON \
		-DZ3_ROOT=$DIR/z3 \
		-DSTORM_USE_INTELTBB=ON -DTBB_ROOT="$TBB_PREFIX" \
		-DSTORM_USE_GUROBI=ON \
		-DSTORM_CARL_DIR_HINT=$DIR/carl/build
fi
# Build Storm binaries
make binaries -j$CORES
echo "# Building Storm finished."
