#!/usr/bin/php
<?php

if ($argc < 2) {
	echo "Please call with the name of the module you want to load, such as 'cmake'".PHP_EOL;
	exit(2);
}

$tool = $argv[1];
switch ($tool) {
	case 'boost':
		printMostRecentVersion('/usr/local_rwth/modules/modulefiles/linux/x86-64/LIBRARIES/boost', $tool);
		break;
	case 'clang':
		printMostRecentVersion('/usr/local_rwth/modules/modulefiles/linux/x86-64/DEVELOP/clang', $tool);
		break;
	case 'cmake':
		printMostRecentVersion('/usr/local_rwth/modules/modulefiles/linux/x86-64/DEVELOP/cmake', $tool);
		break;
	case 'gcc':
		printMostRecentVersion('/usr/local_rwth/modules/modulefiles/linux/x86-64/DEVELOP/gcc', $tool);
		break;
	case 'gurobi':
		printMostRecentVersion('/usr/local_rwth/modules/modulefiles/linux/x86-64/MATH/gurobi', $tool);
		break;
	default:
		echo "Error: Unhandled module '".$tool."' requested!".PHP_EOL;
		exit(3);
}

exit(0);

function printMostRecentVersion($path, $tool) {
	$versions = scanFolderForVersions($path);
	if (count($versions) == 0) {
		echo "Error: The requested module '".$tool."' has NO versions available!".PHP_EOL;
		exit(4);
	}
	
	echo selectVersion($versions);
}

function scanFolderForVersions($folder) {
	$files = scandir($folder);
	$versions = array();
	foreach ($files as $item) {
		if ($item == '.' || $item == '..') {
			continue;
		} else if ($item == '.version' || $item == 'system-default') {
			continue;
		} else if (is_dir($folder.DIRECTORY_SEPARATOR.$item)) {
			continue;
		} else {
			$versions[] = $item;
		}
	}
	
	return $versions;
}

function selectVersion($versions) {
	if (count($versions) == 1) {
		return $versions[0];
	} else {
		rsort($versions);
		return $versions[0];
	}
}
